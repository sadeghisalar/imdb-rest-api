<?php
namespace salars\src\IMDB\structure;
use salars\src\IMDB\core\IMDB;
use phpDocumentor\Reflection\Types\Mixed_;
use Symfony\Component\DomCrawler\Crawler;

interface IMDBInterface
{
    public function fetch($params):string;
    public function parse($html = null):Crawler;
    public function pull($html = null):IMDB;
    public function push($data):IMDB;
    public function get():array;
    public function toArray():array;
    public function toJson():string;
}
