<?php
namespace salars\src\IMDB\structure;
use Symfony\Component\DomCrawler\Crawler;

interface PullParserInterface extends NodeInterface
{

    public function filter(Crawler $crawler);

}
