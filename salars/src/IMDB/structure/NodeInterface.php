<?php
namespace salars\src\IMDB\structure;
interface NodeInterface
{

    public function ID($value);
    public function name($value);
    public function year($value);
    public function genre($value);
    public function summary($value);
    public function director($value);
    public function stars($value);
    public function rate($value);
    public function metascore($value);
    public function votes($value);
    public function poster($value);
    public function certificate($value);
    public function runtime($value);
    public function gross($value);
    public function link($value);

    //Pagination
    public function next($value);
    public function prev($value);
    public function titles($value);

}
