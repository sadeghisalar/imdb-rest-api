<?php
namespace salars\src\IMDB\core;
use salars\src\IMDB\structure\NodeInterface;

abstract class IMDBNode implements NodeInterface
{

    private array $list;

    public function ID($value){
        return $this->setByKey('id',$value);
    }

    public function name($value){
        return $this->setByKey('name',$value);
    }

    public function year($value){
        // TODO: Implement year() method.
        return $this->setByKey('year',$value);
    }

    public function summary($value){
        return $this->setByKey('summary',$value);
    }

    public function link($value){
        return $this->setByKey('link',$value);
    }

    public function rate($value){
        return $this->setByKey('rate',$value);
    }

    public function metascore($value){
        // TODO: Implement metascore() method.
        return $this->setByKey('metascore',$value);
    }

    public function votes($value){
        // TODO: Implement votes() method.
        return $this->setByKey('votes',$value);
    }

    public function poster($value){
        return $this->setByKey('poster',$value);
    }

    public function certificate($value){
        // TODO: Implement certificate() method.
        return $this->setByKey('certificate',$value);
    }

    public function runtime($value){
        // TODO: Implement runtime() method.
        return $this->setByKey('runtime',$value);
    }

    public function genre($value){
        // TODO: Implement genre() method.
        return $this->setByKey('genre',$value);
    }

    public function director($value){
        // TODO: Implement director() method.
        return $this->setByKey('director',$value);
    }

    public function stars($value){
        // TODO: Implement stars() method.
        return $this->setByKey('stars',$value);
    }

    public function gross($value){
        // TODO: Implement gross() method.
        return $this->setByKey('gross',$value);
    }

    public function next($value){
        return $this->setByKey('next',$value);
    }

    public function prev($value){
        return $this->setByKey('prev',$value);
    }

    public function titles($value){
        return $this->setByKey('titles',$value);
        // TODO: Implement titles() method.
    }

    public function new(){
        return new $this();
    }

    public function getList(): array{
        return $this->list;
    }
    public function setList(array $list): void{
        $this->list = $list;
    }
    protected function setByKey($key, $value){
        $this->list[$key] = $value;
        return $this;
    }
}
