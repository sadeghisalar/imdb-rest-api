<?php
namespace salars\src\IMDB\core;
use salars\src\IMDB\structure\PullParserInterface;
use PhpParser\Node\Expr\ClosureUse;
use Symfony\Component\DomCrawler\Crawler;

abstract class PullParser implements PullParserInterface
{
    private array $regex;

    public function getRegex(){
        return $this->regex;
    }
    public function setRegex($regex): void{
        $this->regex = $regex;
    }

    public function filter(Crawler $crawler)
    {
        return $crawler->filter('.lister-list')->first()->filter('.lister-item');
        // TODO: Implement filter() method.
    }

    public function ID($filter)
    {
        preg_match_all($this->regex['movieID'], $filter->filter('.lister-item-image.float-left a')->first()->attr('href'), $matches, PREG_SET_ORDER, 0);
        return $matches[0][0];
        // TODO: Implement ID() method.
    }

    public function name($filter)
    {
        $name = $filter->filter('.lister-item-content > h3.lister-item-header > a')->first()->text();
        return $name;
        // TODO: Implement name() method.
    }

    public function year($filter)
    {
        $year = $filter->filter('.lister-item-year')->first()->text();
        return $year;
        // TODO: Implement year() method.
    }

    public function genre($filter)
    {
        try {
            $genre = $filter->filter('.genre')->first()->text();
        } catch (\Exception $exception) {
            $genre = null;
        }
        return $genre;
        // TODO: Implement genre() method.
    }

    public function summary($filter)
    {
        $summary = $filter->filter('.text-muted')->eq(2)->text();
        return $summary;
        // TODO: Implement summary() method.
    }

    public function director($filter)
    {
        $self = $this;
        $directors_filter = $filter->filter('.lister-item-content p:not(.text-muted)');

        if (strpos($directors_filter->outerHtml(), 'Stars:') != false) {
            $directors = explode('Stars:', $directors_filter->outerHtml());
        }elseif (strpos($directors_filter->outerHtml(), 'Star:') != false) {
            $directors = explode('Star:', $directors_filter->outerHtml());
        }elseif (strpos($directors_filter->outerHtml(), 'Director:') != false) {
            $directors = explode('Director:', $directors_filter->outerHtml());
        }elseif (strpos($directors_filter->outerHtml(), 'Directors:') != false) {
            $directors = explode('Directors:', $directors_filter->outerHtml());
        }

        if (strpos($directors_filter->outerHtml(), 'Director') != false) {
            $directors = str_replace(['Director:', 'Directors:'], '', $directors[0]);
            $parse_director = new Crawler($directors);
            $directors = [];
            $parse_director->filter('a')->each(function ($_director) use ($self, &$directors) {
                $temp = [];
                preg_match_all($self->regex['director_stars_ID'], $_director->attr('href'), $director_stars_ID, PREG_SET_ORDER, 0);
                $temp['name'] = $_director->text();
                $temp['id'] = $director_stars_ID[0][1];
                array_push($directors, $temp);
            });
            try {
                $Director = $directors;
            } catch (\Exception $exception) {
                $Director = null;
            }
        } else {
            $Director = null;
        }
        return $Director;
        // TODO: Implement director() method.
    }

    public function stars($filter)
    {
        $self = $this;
        $stars_filter = $filter->filter('.lister-item-content p:not(.text-muted)');

        if (strpos($stars_filter->outerHtml(), 'Stars:') != false) {
            $stars = explode('Stars:', $stars_filter->outerHtml());
        }elseif (strpos($stars_filter->outerHtml(), 'Star:') != false){
            $stars = explode('Star:', $stars_filter->outerHtml());
        }

        if (strpos($stars_filter->outerHtml(), 'Stars') != false) {
            $stars = str_replace(['Stars:', 'Stars:'], '', $stars[1]);
            //print_r($stars);
            $parse_stars = new Crawler($stars);
            $stars = [];
            $parse_stars->filter('a')->each(function ($_stars) use ($self, &$stars) {
                $temp = [];
                preg_match_all($self->regex['director_stars_ID'], $_stars->attr('href'), $director_stars_ID, PREG_SET_ORDER, 0);
                $temp['name'] = $_stars->text();
                $temp['id'] = $director_stars_ID[0][1];
                array_push($stars, $temp);
            });
            try {
                $Stars = $stars;
            } catch (\Exception $exception) {
                $Stars = null;
            }
        } else {
            $Stars = null;
        }
        return $Stars;
        // TODO: Implement stars() method.
    }

    public function rate($filter)
    {
        try {
            $rate = $filter->filter('.ratings-imdb-rating strong')->first()->text();
        } catch (\Exception $exception) {
            $rate = 0;
        }
        return $rate;
        // TODO: Implement rate() method.
    }

    public function metascore($filter)
    {
        try {
            $metascore = $filter->filter('.metascore')->first()->text();
        } catch (\Exception $exception) {
            $metascore = 0;
        }
        return $metascore;
        // TODO: Implement metascore() method.
    }

    public function votes($filter)
    {
        $votes = $filter->filter('.sort-num_votes-visible span');
        try {
            $votes = $votes->eq(1)->text();
        } catch (\Exception $exception) {
            $votes = null;
        }
        return $votes;
        // TODO: Implement votes() method.
    }

    public function poster($filter)
    {
        preg_match_all($this->regex['movieImage'], $filter->filter('.lister-item-image.float-left a img')->first()->attr('loadlate'), $img, PREG_SET_ORDER, 0);
        if (isset($img[0][0])) {
            $full_poster = str_replace($img[0][0], '_V1_.jpg', $filter->filter('.lister-item-image.float-left a img')->first()->attr('loadlate'));
            $large_poster = str_replace($img[0][0], '_V1_SY1000_CR0,0,665,1000_AL.jpg', $filter->filter('.lister-item-image.float-left a img')->first()->attr('loadlate'));
        } else {
            $full_poster = null;
            $large_poster = null;
        }
        return [
            'full' => $full_poster,
            'large' => $large_poster,
            'thumb' => $filter->filter('.lister-item-image.float-left a img')->first()->attr('loadlate')
        ];
        // TODO: Implement poster() method.
    }

    public function certificate($filter)
    {
        try {
            $certificate = $filter->filter('.certificate')->first()->text();
        } catch (\Exception $exception) {
            $certificate = null;
        }
        return $certificate;
        // TODO: Implement certificate() method.
    }

    public function runtime($filter)
    {
        try {
            $runtime = $filter->filter('.runtime')->first()->text();
        } catch (\Exception $exception) {
            $runtime = null;
        }
        return $runtime;
        // TODO: Implement runtime() method.
    }

    public function gross($filter)
    {
        $gross = $filter->filter('.sort-num_votes-visible span');
        try {
            $gross = $gross->eq(4)->text();
        } catch (\Exception $exception) {
            $gross = null;
        }
        return $gross;
        // TODO: Implement gross() method.
    }

    public function link($filter)
    {
        $link = 'https://www.imdb.com' . $filter->filter('.lister-item-image.float-left a')->first()->attr('href');
        return $link;
        // TODO: Implement link() method.
    }

    public function next($filter)
    {
        try {
            $str = $filter->filter('div.desc')->last()->text();
            preg_match_all($this->regex['pagination'], $str, $next, PREG_SET_ORDER, 0);
            try {
                $str = (int)explode('-', trim($next[0][1]))[1];
            } catch (\Exception $exception) {
                $str = null;
            }
        } catch (\Exception $exception) {
            $str = null;
        }
        return $str;
        // TODO: Implement next() method.
    }

    public function prev($filter)
    {
        try {
            $str = $filter->filter('div.desc')->last()->text();
            preg_match_all($this->regex['pagination'], $str, $next, PREG_SET_ORDER, 0);
        } catch (\Exception $exception) {
            $str = null;
        }
        try {
            $str = (int)explode('-', trim($next[0][1]))[0];
        } catch (\Exception $exception) {
            $str = null;
        }
        return $str;
        // TODO: Implement prev() method.
    }

    public function titles($filter)
    {
        try {
            $str = $filter->filter('div.desc')->last()->text();
            if (strpos($str, 'of') !== false) {
                preg_match_all($this->regex['titles_count_a'], $str, $count, PREG_SET_ORDER, 0);
                $str = (int)str_replace(',', '', $count[0][1]);
            } else {
                preg_match_all($this->regex['titles_count_b'], $str, $count, PREG_SET_ORDER, 0);
                $str = (int)str_replace(',', '', $count[0][1]);
            }
        } catch (\Throwable $th) {
            return 0;
        }
        return $str;
        // TODO: Implement titles() method.
    }
}
