<?php
namespace salars\src\IMDB\core;
use salars\src\IMDB\structure\IMDBInterface;
use salars\src\IMDB\structure\NodeInterface;
use Symfony\Component\DomCrawler\Crawler;

abstract class IMDB implements IMDBInterface
{

    private Crawler $crawler;
    private IMDBNode $node;
    private PullParser $pullParser;
    private string $url;
    private array $params;
    private string $html;
    private array $data;
    private array $keys;

    public function __construct(array $params, NodeInterface $node, PullParser $pullParser){
        $this->setParams($params);
        $this->setNode($node);
        $this->setPullParser($pullParser);
        $this->fetch($this->params);
    }

    public function fetch($params): string{
        // TODO: Implement fetch() method.
        $this->setHtml(file_get_contents($this->url.http_build_query($params)));
        return $this->getHtml();
    }

    public function parse($html = null): Crawler{
        // TODO: Implement parse() method.
        $this->setCrawler(new Crawler($this->getHtml()));
        return $this->getCrawler();
    }

    public function pull($html = null): IMDB{
        // TODO: Implement pull() method.
        $self = $this;
        $return = [];
        $return[$this->keys[0]] = [];

        $crawler = $this->parse($this->html);

        $filter = $this->getPullParser()->filter($crawler);

        $filter->each(function (Crawler $item, $i) use ($self,&$return) {
            $node = $self->getNode();
            $pullParser = $self->getPullParser();

            $node->ID($pullParser->ID($item));
            $node->name($pullParser->name($item));
            $node->year($pullParser->year($item));
            $node->certificate($pullParser->certificate($item));
            $node->runtime($pullParser->runtime($item));
            $node->poster($pullParser->poster($item));
            $node->genre($pullParser->genre($item));
            $node->summary($pullParser->summary($item));
            $node->link($pullParser->link($item));
            $node->rate($pullParser->rate($item));
            $node->metascore($pullParser->metascore($item));
            $node->director($pullParser->director($item));
            $node->stars($pullParser->stars($item));
            $node->votes($pullParser->votes($item));
            $node->gross($pullParser->gross($item));

            array_push($return[$self->keys[0]],$node->getList());
        });

        $node = $this->newNode();
        $node->next($this->getPullParser()->next($crawler));
        $node->prev($this->getPullParser()->prev($crawler));
        $node->titles($this->getPullParser()->titles($crawler));
        $return[$this->keys[1]]= $node->getList();

        return $this->push($return);
    }

    public function push($data): IMDB{
        $this->setData($data);
        return $this;
    }

    public function get(): array{
        // TODO: Implement get() method.
        return $this->getData();
    }

    public function toArray(): array{
        // TODO: Implement output() method.
        return $this->get();
    }

    public function toJson(): string{
        // TODO: Implement toJson() method.
        return json_encode($this->get());
    }


    public function getKeys(): array{
        return $this->keys;
    }
    public function setKeys(array $keys): void{
        $this->keys = $keys;
    }

    public function getData(): array{
        return $this->data;
    }
    public function setData(array $data): void{
        $this->data = $data;
    }

    public function getNode(): NodeInterface{
        return $this->node;
    }
    public function newNode(): NodeInterface{
        return $this->getNode()->new();
    }
    public function setNode(NodeInterface $node): void{
        $this->node = $node;
    }

    public function getUrl(): string{
        return $this->url;
    }
    public function setUrl(string $url): void{
        $this->url = $url;
    }

    public function getPullParser(): PullParser{
        return $this->pullParser;
    }
    public function setPullParser(PullParser $pullParser): void{
        $this->pullParser = $pullParser;
    }

    public function getParams(): array{
        return $this->params;
    }
    public function setParams($params): void{
        $this->params = $params;
    }

 
    public function getHtml(): string{
        return $this->html;
    }
    public function setHtml($html): void{
        $this->html = $html;
    }


    public function getCrawler(): Crawler{
        return $this->crawler;
    }
    public function setCrawler($crawler): void{
        $this->crawler = $crawler;
    }
}
