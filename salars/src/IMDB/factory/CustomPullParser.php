<?php
namespace salars\src\IMDB\factory;
use salars\src\IMDB\core\PullParser;

class CustomPullParser extends PullParser
{

    public function __construct(){
        $this->setRegex([
            'movieID' => '/(\w\w\w\w\w\w)\d\w+/m',
            'movieImage' => '/(_V1_)(.)+/m',
            'director_stars_ID' => '/\/name\/(.*)\//m',
            'integer' => '/\d/m',
            'pagination' => '/(.*?)of(.*)/m',
            'titles_count_a' => '/of(.*)titles/m',
            'titles_count_b' => '/(.*)titles/m',
        ]);
    }

}
