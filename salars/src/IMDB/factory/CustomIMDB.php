<?php
namespace salars\src\IMDB\factory;
use salars\src\IMDB\core\IMDB;

class CustomIMDB extends IMDB{

    public function __construct(array $params)
    {
        $this->setUrl('https://www.imdb.com/search/title/?');
        $this->setKeys(['result','pagination']);
        parent::__construct($params,new CustomIMDBNode(),new CustomPullParser());
    }

}
