<?php
namespace salars\src\IMDB\factory;
use salars\src\IMDB\core\IMDBNode;

class CustomIMDBNode extends IMDBNode
{

    public function year($value){
        $value = str_replace(['(',')'],'',trim($value));
        return parent::year($value); // TODO: Change the autogenerated stub
    }

    public function rate($value){
        $value = (double) trim($value);
        return parent::rate($value); // TODO: Change the autogenerated stub
    }

    public function metascore($value){
        $value = (int) trim($value);
        return parent::metascore($value); // TODO: Change the autogenerated stub
    }

    public function votes($value){
        $value = (int) str_replace(',','',trim($value));
        return parent::votes($value); // TODO: Change the autogenerated stub
    }

    public function genre($value)
    {
        $value = explode(",",$value);
        $value = array_map(function($n){
            return trim($n);
        },$value);
        return parent::genre($value); // TODO: Change the autogenerated stub
    }

}
