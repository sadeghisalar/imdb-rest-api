# IMDB REST API
*https://www.imdb.com/search/title/* To Rest API

# Get Started

### install via Composer
```php
 composer require salars/imdb-rest-api
```
### Example
```php
    require_once __DIR__."/vendor/autoload.php";
    use salars\src\IMDB\factory\CustomIMDB;

    $params = [
        'groups' => 'top_100'
    ];

    $imdb = new CustomIMDB($params);
    echo $imdb->pull()->toJson();

```